package com.chen.gift.client;

import com.chen.gift.domain.LoginUser;
import com.chen.gift.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "AUTH-SERVICE", fallbackFactory = AuthClientFallbackFactory.class)
public interface AuthClient {

    @PutMapping("/loginUser")
    AjaxResult addOrUpdate(@RequestBody LoginUser loginUser);

}
