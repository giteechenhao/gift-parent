package com.chen.gift.client;

import com.chen.gift.domain.LoginUser;
import com.chen.gift.util.AjaxResult;
import feign.hystrix.FallbackFactory;

public class AuthClientFallbackFactory implements FallbackFactory<AuthClient> {
    @Override
    public AuthClient create(Throwable throwable) {
        return new AuthClient() {
            @Override
            public AjaxResult addOrUpdate(LoginUser loginUser) {
                return AjaxResult.me()
                        .setSuccess(false);
            }
        };
    }
}
