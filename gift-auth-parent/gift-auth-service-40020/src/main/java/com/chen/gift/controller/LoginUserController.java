package com.chen.gift.controller;

import com.chen.gift.service.ILoginUserService;
import com.chen.gift.domain.LoginUser;
import com.chen.gift.query.LoginUserQuery;
import com.chen.gift.util.AjaxResult;
import com.chen.gift.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/loginUser")
public class LoginUserController {

    @Autowired
    public ILoginUserService loginUserService;

    /**
     * 保存和修改公用的
     * @param loginUser  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody LoginUser loginUser){
        try {
            if( loginUser.getId()!=null)
                loginUserService.updateById(loginUser);
            else
                loginUserService.insert(loginUser);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }

    /**
    * 删除对象信息
    * @param id 根据id删除数据
    * @return AjaxResult转换结果
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            loginUserService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    /**
    * @param id 根据id获取用户
    * @return 返回根据id获取的用户
    */
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id) {
        try {
            LoginUser loginUser = loginUserService.selectById(id);
            return AjaxResult.me().setResultObj(loginUser);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return 所有员工数据
    */
    @GetMapping
    public AjaxResult list(){
        try {
            List< LoginUser> list = loginUserService.selectList(null);
            return AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }

    /**
    * 分页查询数据
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public AjaxResult json(@RequestBody LoginUserQuery query) {
        try {
            Page<LoginUser> page = new Page<>(query.getPage(),query.getRows());
            page = loginUserService.selectPage(page);
            PageList<LoginUser> pageList = new PageList<>(page.getTotal(),page.getRecords());
            return AjaxResult.me().setResultObj(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }

    }
}
