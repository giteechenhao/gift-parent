package com.chen.gift.mapper;

import com.chen.gift.domain.LoginUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface LoginUserMapper extends BaseMapper<LoginUser> {

}
