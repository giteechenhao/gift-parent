package com.chen.gift.mapper;

import com.chen.gift.domain.Post;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 岗位信息表 Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface PostMapper extends BaseMapper<Post> {

}
