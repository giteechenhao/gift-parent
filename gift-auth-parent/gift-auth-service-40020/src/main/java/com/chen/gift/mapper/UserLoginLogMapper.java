package com.chen.gift.mapper;

import com.chen.gift.domain.UserLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 登录记录 Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}
