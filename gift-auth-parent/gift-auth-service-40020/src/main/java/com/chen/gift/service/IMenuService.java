package com.chen.gift.service;

import com.chen.gift.domain.Menu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface IMenuService extends IService<Menu> {

}
