package com.chen.gift.service;

import com.chen.gift.domain.Post;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 岗位信息表 服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface IPostService extends IService<Post> {

}
