package com.chen.gift.service;

import com.chen.gift.domain.UserLoginLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 登录记录 服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface IUserLoginLogService extends IService<UserLoginLog> {

}
