package com.chen.gift.service.impl;

import com.chen.gift.domain.LoginUser;
import com.chen.gift.mapper.LoginUserMapper;
import com.chen.gift.service.ILoginUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class LoginUserServiceImpl extends ServiceImpl<LoginUserMapper, LoginUser> implements ILoginUserService {

}
