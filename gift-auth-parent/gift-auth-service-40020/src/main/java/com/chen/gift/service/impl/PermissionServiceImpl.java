package com.chen.gift.service.impl;

import com.chen.gift.domain.Permission;
import com.chen.gift.mapper.PermissionMapper;
import com.chen.gift.service.IPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
