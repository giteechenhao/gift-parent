package com.chen.gift.service.impl;

import com.chen.gift.domain.Role;
import com.chen.gift.mapper.RoleMapper;
import com.chen.gift.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
