package com.chen.gift.exception.ass;

import com.chen.gift.exception.code.ErrorCode;
import com.chen.gift.exception.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * 断言类
 * 抽取断言工具进行数据判断，然后抛出异常
 */
public class ChenAssert {

    /**
     * @param str 断言为blank,不为blank就报错
     * @param message 错误信息
     */
    public static void isBlank(String str, String message) {
        if (StringUtils.isNotBlank(str))
            // 抛出自定义异常错误
            throw new BusinessException(message);
    }

    /**
     * 支持错误码
     * @param str 断言为blank,不为blank就报错
     * @param errorCode 整个系统的错误码和错误信息
     */
    public static void isBlank(String str, ErrorCode errorCode) {
        if (StringUtils.isNotBlank(str))
            // 抛出自定义异常错误
            throw new BusinessException(errorCode.getMessage(),errorCode.getCode());
    }

    /**
     * @param str 断言不为blank,为blank就报错
     * @param message 错误信息
     */
    public static void isNotBlank(String str, String message) {
        if (StringUtils.isBlank(str))
            // 抛出自定义异常错误
            throw new BusinessException(message);
    }

    /**
     * 支持错误码
     * @param str 断言不为blank,为blank就报错
     * @param errorCode 整个系统的错误码和错误信息
     */
    public static void isNotBlank(String str, ErrorCode errorCode) {
        if (StringUtils.isBlank(str))
            // 抛出自定义异常错误
            throw new BusinessException(errorCode.getMessage(),errorCode.getCode());
    }

    /**
     * @param objects 断言数组为blank,不为blank就报错
     * @param message 错误信息
     */
    public static void isEmpty(List<?> objects, String message) {
        if (objects != null || objects.size() > 0)
            // 抛出自定义异常错误
            throw new BusinessException(message);
    }

    /**
     * 支持错误码
     * @param objects 断言数组为blank,不为blank就报错
     * @param errorCode 整个系统的错误码和错误信息
     */
    public static void isEmpty(List<?> objects, ErrorCode errorCode) {
        if (objects != null || objects.size() > 0)
            // 抛出自定义异常错误
            throw new BusinessException(errorCode.getMessage(), errorCode.getCode());
    }

    /**
     * @param objects 断言数组不为blank,为blank就报错
     * @param message 错误信息
     */
    public static void isNotEmpty(List<?> objects, String message) {
        if (objects == null || objects.size() < 1)
            // 抛出自定义异常错误
            throw new BusinessException(message);
    }

    /**
     * 支持错误码
     * @param objects 断言数组不为blank,为blank就报错
     * @param errorCode 整个系统的错误码和错误信息
     */
    public static void isNotEmpty(List<?> objects, ErrorCode errorCode) {
        if (objects == null || objects.size() < 1)
            // 抛出自定义异常错误
            throw new BusinessException(errorCode.getMessage(), errorCode.getCode());
    }

}
