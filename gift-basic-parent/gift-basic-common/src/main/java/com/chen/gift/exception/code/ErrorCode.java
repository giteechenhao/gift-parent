package com.chen.gift.exception.code;

/**
 * 整个系统中的错误枚举
 */
public enum ErrorCode {
    ERROR_CODE_TENANT_EXIST(40001,"该租户已经存在!请直接登陆使用!"),
    ERROR_CODE_TENANT_ADMIN_EXIST(40002,"该管理员已经存在!请直接登陆再添加租户!"),
    ERROR_CODE_400(400,"参数错误"),
    ERROR_CODE_500(500,"系统异常!请联系管理员!");
    private Integer code;
    private String message;

    ErrorCode() {
    }

    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
