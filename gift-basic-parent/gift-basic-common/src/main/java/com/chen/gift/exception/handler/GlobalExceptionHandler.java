package com.chen.gift.exception.handler;

import com.chen.gift.exception.code.ErrorCode;
import com.chen.gift.exception.exception.BusinessException;
import com.chen.gift.util.AjaxResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

// 全局异常处理
@RestControllerAdvice
public class GlobalExceptionHandler{

    /**
     * 捕获BusinessException的异常
     * @param e BusinessException对象
     * @return 返回错误信息
     */
    @ExceptionHandler(BusinessException.class)
    public AjaxResult businessExceptionHandler(BusinessException e) {
        e.printStackTrace();    // 可预见的异常
        return AjaxResult.me()
                .setSuccess(false)
                .setStatusCode(e.getErrorCode())
                .setMessage(e.getMessage());
    }

    /**
     * 捕获MethodArgumentNotValidException的异常
     * @param e 方法参数无效异常 对象
     * @return 返回错误信息
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public AjaxResult methodArgumentNotValidException(MethodArgumentNotValidException e) {
        e.printStackTrace(); // 可预见的异常
        BindingResult bindingResult = e.getBindingResult();
        // 获取所有的字段验证错误数据
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        // 拼接所有字段验证错误信息
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fieldErrors.size(); i++) {
            FieldError error = fieldErrors.get(0);
            String message = error.getDefaultMessage();
            if (i==fieldErrors.size()-1)
                sb.append(message);
            else
                sb.append(message).append(",");
        }
        return AjaxResult.me()
                .setSuccess(false)
                .setStatusCode(ErrorCode.ERROR_CODE_400.getCode())
                .setMessage(ErrorCode.ERROR_CODE_400.getMessage()+":"+ sb);
    }

    /**
     * 捕获Exception
     * @param e 异常对象
     * @return 返回错误信息
     */
    @ExceptionHandler(Exception.class)
    public AjaxResult exceptionHandler(Exception e) {
        e.printStackTrace();
        return AjaxResult.me()
                .setSuccess(false)
                .setStatusCode(ErrorCode.ERROR_CODE_500.getCode())
                .setMessage(ErrorCode.ERROR_CODE_400.getMessage());
    }

}
