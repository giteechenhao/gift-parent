package com.chen.gift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServiceStartApp {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServiceStartApp.class, args);
    }

}
