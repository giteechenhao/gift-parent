package com.chen.gift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient  // //标识是Eureka服务端
@EnableZuulProxy    // 网关服务
public class ZuulServiceStartApp {

    public static void main(String[] args) {
        SpringApplication.run(ZuulServiceStartApp.class, args);
    }

}
