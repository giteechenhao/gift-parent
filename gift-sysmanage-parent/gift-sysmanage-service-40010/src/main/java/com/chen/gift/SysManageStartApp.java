package com.chen.gift;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.chen.gift.mapper")
@EnableFeignClients
public class SysManageStartApp {

    public static void main(String[] args) {
        SpringApplication.run(SysManageStartApp.class, args);
    }

}
