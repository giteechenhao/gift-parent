package com.chen.gift.controller;

import com.chen.gift.service.IConfigService;
import com.chen.gift.domain.Config;
import com.chen.gift.query.ConfigQuery;
import com.chen.gift.util.AjaxResult;
import com.chen.gift.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    public IConfigService configService;

    /**
     * 保存和修改公用的
     * @param config  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Config config){
        try {
            if( config.getConfigId()!=null)
                configService.updateById(config);
            else
                configService.insert(config);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }

    /**
    * 删除对象信息
    * @param id 根据id删除数据
    * @return AjaxResult转换结果
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            configService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    /**
    *
    * @param id 根据id获取用户
    * @return 返回根据id获取的用户
    */
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id) {
        try {
            Config config = configService.selectById(id);
            return AjaxResult.me().setResultObj(config);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return 所有员工数据
    */
    @GetMapping
    public AjaxResult list(){
        try {
            List< Config> list = configService.selectList(null);
            return AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }

    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public AjaxResult json(@RequestBody ConfigQuery query) {
        try {
            Page<Config> page = new Page<>(query.getPage(),query.getRows());
            page = configService.selectPage(page);
            PageList<Config> pageList = new PageList<>(page.getTotal(),page.getRecords());
            return AjaxResult.me().setResultObj(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }

    }
}
