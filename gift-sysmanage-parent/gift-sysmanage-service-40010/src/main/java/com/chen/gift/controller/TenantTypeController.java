package com.chen.gift.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.chen.gift.service.ITenantTypeService;
import com.chen.gift.domain.TenantType;
import com.chen.gift.query.TenantTypeQuery;
import com.chen.gift.util.AjaxResult;
import com.chen.gift.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tenantType")
public class TenantTypeController {

    @Autowired
    public ITenantTypeService tenantTypeService;

    /**
     * 保存和修改公用的
     * @param tenantType  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody TenantType tenantType){
        try {
            if( tenantType.getId()!=null)
                tenantTypeService.updateById(tenantType);
            else
                tenantTypeService.insert(tenantType);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }

    /**
    * 删除对象信息
    * @param id 根据id删除数据
    * @return AjaxResult转换结果
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            tenantTypeService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    /**
    *
    * @param id 根据id获取用户
    * @return 返回根据id获取的用户
    */
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id) {
        try {
            TenantType tenantType = tenantTypeService.selectById(id);
            return AjaxResult.me().setResultObj(tenantType);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return 所有员工数据
    */
    @GetMapping
    public AjaxResult list(){
        try {
            List< TenantType> list = tenantTypeService.selectList(null);
            return AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }

    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public AjaxResult json(@RequestBody TenantTypeQuery query) {
        try {
            Page<TenantType> page = new Page<>(query.getPage(),query.getRows());
            // 高级查询
            EntityWrapper<TenantType> wrapper = null;
            if (StrUtil.isNotBlank(query.getKeyword())) {
                wrapper = new EntityWrapper<>();
                wrapper.like("name", query.getKeyword())
                        .or().like("description", query.getKeyword());
            }
//            page = tenantTypeService.selectPage(page);
            page = tenantTypeService.selectPage(page, wrapper);
            //分装mybatisplus的分页数据为我们自己PageList
            PageList<TenantType> pageList = new PageList<>(page.getTotal(),page.getRecords());
            return AjaxResult.me().setResultObj(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }

    }
}
