package com.chen.gift.mapper;

import com.chen.gift.domain.Config;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
