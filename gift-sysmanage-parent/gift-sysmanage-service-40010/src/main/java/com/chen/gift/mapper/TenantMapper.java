package com.chen.gift.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.chen.gift.domain.Tenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.chen.gift.query.TenantQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface TenantMapper extends BaseMapper<Tenant> {

    List<Tenant> loadPageList(Page<Tenant> page, TenantQuery query);

    void saveMeals(@Param("tenantId") Long tenantId,@Param("mealId") Long mealId);
}
