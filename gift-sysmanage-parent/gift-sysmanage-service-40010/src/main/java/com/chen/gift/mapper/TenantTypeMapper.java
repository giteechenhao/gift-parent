package com.chen.gift.mapper;

import com.chen.gift.domain.TenantType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

}
