package com.chen.gift.service;

import com.chen.gift.domain.Config;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface IConfigService extends IService<Config> {

}
