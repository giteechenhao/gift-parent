package com.chen.gift.service;

import com.chen.gift.domain.Department;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface IDepartmentService extends IService<Department> {

}
