package com.chen.gift.service;

import com.chen.gift.domain.Tenant;
import com.baomidou.mybatisplus.service.IService;
import com.chen.gift.query.TenantQuery;
import com.chen.gift.util.AjaxResult;
import com.chen.gift.util.PageList;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
public interface ITenantService extends IService<Tenant> {

    PageList<Tenant> selectPageList(TenantQuery query);

    AjaxResult settlement(Tenant tenant);
}
