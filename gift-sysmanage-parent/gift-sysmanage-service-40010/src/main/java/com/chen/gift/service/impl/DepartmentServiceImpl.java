package com.chen.gift.service.impl;

import com.chen.gift.domain.Department;
import com.chen.gift.mapper.DepartmentMapper;
import com.chen.gift.service.IDepartmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}
