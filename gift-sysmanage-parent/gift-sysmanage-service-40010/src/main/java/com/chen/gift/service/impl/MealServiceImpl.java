package com.chen.gift.service.impl;

import com.chen.gift.domain.Meal;
import com.chen.gift.mapper.MealMapper;
import com.chen.gift.service.IMealService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class MealServiceImpl extends ServiceImpl<MealMapper, Meal> implements IMealService {

}
