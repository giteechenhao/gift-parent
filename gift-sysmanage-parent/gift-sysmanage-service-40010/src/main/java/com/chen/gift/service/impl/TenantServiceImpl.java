package com.chen.gift.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.chen.gift.client.AuthClient;
import com.chen.gift.domain.Employee;
import com.chen.gift.domain.LoginUser;
import com.chen.gift.domain.Tenant;
import com.chen.gift.exception.ass.ChenAssert;
import com.chen.gift.exception.code.ErrorCode;
import com.chen.gift.mapper.EmployeeMapper;
import com.chen.gift.mapper.TenantMapper;
import com.chen.gift.query.TenantQuery;
import com.chen.gift.service.ITenantService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.chen.gift.util.AjaxResult;
import com.chen.gift.util.PageList;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private AuthClient authClient;

    @Override
    public PageList<Tenant> selectPageList(TenantQuery query) {
        // 分页条件，查询出总条数
        Page<Tenant> page = new Page<>(query.getPage(), query.getRows());
        //跨表查询出来结构
        List<Tenant> tenants = tenantMapper.loadPageList(page,query);
        return new PageList<>(page.getTotal(), tenants);
    }

    @Override
    @Transactional
    public AjaxResult settlement(Tenant tenant) {
        //1 校验
        Employee admin = tenant.getAdmin();
        //1.1 非空校验
        String companyName = tenant.getCompanyName();
        //RonghuaAssert.isNotBlank(companyName,"请输入相关信息后,再入驻!");
        //1.2 租户是否存在
        List<Tenant> tenants = tenantMapper.selectList(
                new EntityWrapper<Tenant>()
                        .eq("company_name", companyName)
        );
        ChenAssert.isEmpty(tenants, ErrorCode.ERROR_CODE_TENANT_EXIST);
        //1.3 ??? 如果管理员已经存在!已经有店铺了! 方案1: 新增店铺用原来的密码还是新的密码!!!!  方案2:登陆进行新增店铺
        String username = admin.getUsername();
        List<Employee> employees = employeeMapper.selectList(
                new EntityWrapper<Employee>()
                        .eq("username", username)
        );
        ChenAssert.isEmpty(tenants,ErrorCode.ERROR_CODE_TENANT_ADMIN_EXIST);
        //2 百度自动审核；审核不通过,跳转到店铺界面,返回前端一个TenantId,让前端查询回显后,修改后继续提交!  @TODO
        boolean auditResult = true;
        //3 保存登陆用户信息,返回loginId
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(loginUser,admin);
//        loginUser.setUsername(admin.getUsername());
//        loginUser.setPassword(admin.getPassword());
//        loginUser.setTel(admin.getTel());
//        loginUser.setEmail(admin.getEmail());
        loginUser.setType(0);
        AjaxResult ajaxResult = authClient.addOrUpdate(loginUser);
        if (!ajaxResult.isSuccess())
            return  ajaxResult;
        Long loginId = Long.parseLong(ajaxResult.getResultObj().toString()) ;

        //4 保存管理员信息  loginId
        admin.setLoginId(loginId);
        admin.setInputTime(new Date());
        admin.setRealName(admin.getUsername());
        admin.setState(0);
        admin.setType(1);
        employeeMapper.insert(admin);

        //5 保存租户信息  adminId
        tenant.setAdminId(admin.getId());
        tenant.setRegisterTime(new Date());
        tenant.setState(auditResult?1:0);
        tenantMapper.insert(tenant);

        //6 保存套餐
        Long tenantId = tenant.getId();
        Long mealId = tenant.getMealId();
        tenantMapper.saveMeals(tenantId,mealId);

        if (!auditResult){
            return AjaxResult.me().setSuccess(false)
                    .setMessage("审核失败!").setResultObj(tenantId);
        }else{
            //7 发送管理员激活邮件   redis存放校验码  生成激活连接放入邮件发送  @TODO
            return AjaxResult.me();
        }
    }
}
