package com.chen.gift.service.impl;

import com.chen.gift.domain.TenantType;
import com.chen.gift.mapper.TenantTypeMapper;
import com.chen.gift.service.ITenantTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-24
 */
@Service
public class TenantTypeServiceImpl extends ServiceImpl<TenantTypeMapper, TenantType> implements ITenantTypeService {

}
