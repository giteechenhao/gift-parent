package com.chen.gift;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.chen.gift.mapper")
public class UserServiceStartApp {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceStartApp.class, args);
    }

}
