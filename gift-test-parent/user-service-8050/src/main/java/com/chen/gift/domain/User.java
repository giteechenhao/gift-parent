package com.chen.gift.domain;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-23
 */
@TableName("user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;
    @TableField("user_name")
    private String userName;
    @TableField("user_pass")
    private String userPass;
    @TableField("real_name")
    private String realName;
    /**
     * 1：男  2：女
     */
    @TableField("user_sex")
    private Integer userSex;
    @TableField("user_age")
    private Integer userAge;
    @TableField("user_nation")
    private String userNation;
    @TableField("user_level")
    private String userLevel;
    @TableField("depart_id")
    private Integer departId;
    @TableField("user_pose")
    private String userPose;
    @TableField("reg_date")
    private Date regDate;
    @TableField("leave_date")
    private Date leaveDate;
    @TableField("leave_reason")
    private String leaveReason;
    /**
     * 1-实习 2-正式员工 3,4-申请审核中 5-申请转正
     */
    private Integer statue;
    /**
     * 1-在职 2-离职
     */
    @TableField("user_type")
    private Integer userType;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getUserSex() {
        return userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

    public String getUserNation() {
        return userNation;
    }

    public void setUserNation(String userNation) {
        this.userNation = userNation;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    public Integer getDepartId() {
        return departId;
    }

    public void setDepartId(Integer departId) {
        this.departId = departId;
    }

    public String getUserPose() {
        return userPose;
    }

    public void setUserPose(String userPose) {
        this.userPose = userPose;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public Integer getStatue() {
        return statue;
    }

    public void setStatue(Integer statue) {
        this.statue = statue;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "User{" +
        ", userId=" + userId +
        ", userName=" + userName +
        ", userPass=" + userPass +
        ", realName=" + realName +
        ", userSex=" + userSex +
        ", userAge=" + userAge +
        ", userNation=" + userNation +
        ", userLevel=" + userLevel +
        ", departId=" + departId +
        ", userPose=" + userPose +
        ", regDate=" + regDate +
        ", leaveDate=" + leaveDate +
        ", leaveReason=" + leaveReason +
        ", statue=" + statue +
        ", userType=" + userType +
        "}";
    }
}
