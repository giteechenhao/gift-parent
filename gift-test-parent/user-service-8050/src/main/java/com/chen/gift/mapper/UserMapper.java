package com.chen.gift.mapper;

import com.chen.gift.domain.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-23
 */
public interface UserMapper extends BaseMapper<User> {

}
