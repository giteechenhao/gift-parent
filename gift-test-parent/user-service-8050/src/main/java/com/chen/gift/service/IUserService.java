package com.chen.gift.service;

import com.chen.gift.domain.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ChenHao
 * @since 2023-02-23
 */
public interface IUserService extends IService<User> {

}
