import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'

import axios from 'axios'
// axios.defaults.baseURL = "http://localhost:8080"; // 给Axios设置统一的请求前缀
// axios.defaults.baseURL = "http://api.chen.com"; // 给Axios设置统一的请求前缀
// axios.defaults.baseURL = "http://192.168.3.213:8085"; // 给Axios设置统一的请求前缀
axios.defaults.baseURL = "http://localhost:10090/service"; // 给Axios设置统一的请求前缀
// 给Vue设置一个属性
Vue.prototype.$http = axios

Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
  routes
})
// 登录拦截
// router.beforeEach((to, from, next) => {
//   //NProgress.start();
//   if (to.path == '/login') {
//       localStorage.removeItem('loginUser');
//       localStorage.removeItem('token');
//   }
//   let user = JSON.parse(localStorage.getItem('loginUser'));
//   if (!user && to.path !== '/login' && to.path!=='/recruitment') {
//     next({ path: '/login' })
//   } else {
//     next()
//   }
// })

//router.afterEach(transition => {
//NProgress.done();
//});

// axios请求拦截器:添加请求拦截器，在请求头中加token
// axios.interceptors.request.use(
//     request =>{
//       // 从localStorage中获取token
//       let token = localStorage.getItem("token");
//       // 如果token有值，我们就放到请求头里面
//       if (token) {
//           request.headers.token = token;
//       }
//       return request
//     },
//     error => {
//       return Promise.reject(error)
//     }
// )
//
// // 响应拦截器 - 后端返回来的结果,都要先经过响应拦截器
// axios.interceptors.response.use(response => {
//       // 对返回的数据进行操作
//       let result = response.data; // response.data 就是后端返给我们的数据
//       // 说明未登录,被拦截了,那么就要跳回到登陆页面
//       if (!result.success && result.message == "Not Login") {
//         // 跳转回登陆页面,让用户登陆
//         router.push({path: "/login"});
//       }
//       if(!result.success && result.message == "noPermission"){ // 说明没有权限
//           router.push({ path: '/401' });  // 跳转到统一的401页面
//       }
//       return response
//     },
//     error => {
//       return Promise.reject(error)
//     }
// )
new Vue({
  //el: '#app',
  //template: '<App/>',
  router,
  store,
  //components: { App }
  render: h => h(App)
}).$mount('#app')

