import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import NoPermission from './views/401.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import Form from './views/nav1/Form.vue'
import user from './views/nav1/user.vue'
import Page4 from './views/nav2/Page4.vue'
import echarts from './views/charts/echarts.vue'

import Emplist from './views/org/Emplist.vue'
import Deptlist from "./views/org/Deptlist";
import Shop from "./views/org/Shop";
import Permission from "./views/permission/Permission";
import Role from "./views/permission/Role";
import Config from './views/sysmanage/config'
import Recruitment from "./views/Recruitment";
import CarType from "./views/car/CarType";
import Car from "./views/car/Car";
import TenantType from "./views/sysmanage/TenantType";
import Tenant from "./views/sysmanage/Tenant";

let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/recruitment',
        component: Recruitment,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: '',
        hidden: true,
        leaf: true, // 当前是一个叶子节点,下面没有子菜单
        children: [
            { path: '/401', component: NoPermission, name: '' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: 'Index',
        iconCls: 'fa fa-bar-chart',
        leaf: true,
        children: [
            { path: '/index', component: echarts, name: '首页' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '车辆管理',
        iconCls: 'el-icon-truck',//图标样式class
        children: [
            { path: '/cartype', component: CarType, name: '车辆类型' },
            { path: '/car', component: Car, name: '车辆信息' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '权限管理',
        iconCls: 'fa fa-address-card',//图标样式class
        children: [
            { path: '/permission', component: Permission, name: '权限信息' },
            { path: '/role', component: Role, name: '角色信息' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '系统管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/tenantType', component: TenantType, name: '租户类型' },
            { path: '/tenant', component: Tenant, name: '租户管理' },
            { path: '/tenan', component: Form, name: '租户管理' },
            { path: '/form', component: Form, name: '数据字典' },
            { path: '/user', component: user, name: '数据字典明细' },
            { path: '/config', component: Config, name: '系统配置' },
        ]
    },
    // {
    //     path: '/',
    //     component: Home,
    //     name: '导航一',
    //     iconCls: 'el-icon-message',//图标样式class
    //     children: [
    //         { path: '/main', component: Main, name: '主页', hidden: true },
    //         { path: '/table', component: Table, name: 'Table' },
    //         { path: '/form', component: Form, name: 'Form' },
    //         { path: '/user', component: user, name: '列表' },
    //     ]
    // },
    // {
    //     path: '/',
    //     component: Home,
    //     name: '导航二',
    //     iconCls: 'fa fa-id-card-o',
    //     children: [
    //         { path: '/page4', component: Page4, name: '页面4' },
    //     ]
    // },
    // {
    //     path: '/',
    //     component: Home,
    //     name: '',
    //     iconCls: 'fa fa-address-card',
    //     leaf: true,//只有一个节点
    //     children: [
    //         { path: '/page6', component: Page6, name: '导航三' }
    //     ]
    // },
    {
        path: '/',
        component: Home,
        name: 'Charts',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/echarts', component: echarts, name: 'echarts' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;