// axios.defaults.baseURL = "http://localhost:8080"; // 给Axios设置统一的请求前缀
// axios.defaults.baseURL = "http://api.chen.com"; // 给Axios设置统一的请求前缀
axios.defaults.baseURL = "http://113.125.73.31:81"; // 给Axios设置统一的请求前缀
// 给Vue设置一个属性
Vue.prototype.$http = axios

function urlToObj(url) {
    let paramObj = {};
    if (url.indexOf("?") > 0) {
        url = url.substring(url.indexOf("?")+1)
        console.log(url)
        let paramArray = url.split('&');
        for (let i = 0; i < paramArray.length; i++) {
            let key = paramArray[i].split("=")[0]
            let value = paramArray[i].split("=")[1]
            paramObj[key] = value;
        }
    }
    return paramObj;
}

// 拿到当前页面的url
var url = location.href;    // http://127.0.0.1/about.html

// 定义不需要登陆访问的页面地址
var releasePath = ["index.html","about.html","list.html","binder.html","callback.html","test.html","exam.html"];

// 判断url要访问的页面在不在"不需要登陆访问的页面地址 "里面 在的话就说明不需要登陆 否则就要跳转回首页
url = url.substring(url.lastIndexOf("/")+1);    // about.html
var isRun = false;  // 是否放行
if(!url){
    isRun = true;
}else{
    for(var i=0;i<releasePath.length;i++){
        var path = releasePath[i];
        if(url.indexOf(path) > -1){
            isRun = true;
            break;
        }
    }
}
if(!isRun){ // isRun == false,说明当前要访问的页面不在放行的数组中
    // 当前是否登陆 如果没有登陆的话才拦截
    var loginUser = JSON.parse(localStorage.getItem("loginUser"));
    if(!loginUser){
        alert("当前操作需要登录后才能访问,请登陆后重试!");
        location.href = "/index.html";
    }
}

// 前置拦截
axios.interceptors.request.use(
    config => {
        // 从localStorage中获取token
        let token = localStorage.getItem("token");

        // 如果token有值,我们就放到请求头里面
        if (token) {
            config.headers["token"] = token;
        }
        return config
    },
    error => {
        return Promise.reject(error)
    })

// 响应拦截器 - 后端返回来的结果,都要先经过响应拦截器
axios.interceptors.response.use(function(response){
    //对返回的数据进行操作
    let result = response.data;  // response.data 就是后端返给我们的数据
    if(!result.success && result.message === "Not Login"){ // 说明未登录,被拦截了,那么就要跳回到登陆页面
        alert("当前资源需要登陆后才能访问!")
    }
    return response
},function(err){
    return Promise.reject(err)
})